/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    function scrollToElement(ele) {
        $('html,body').animate({ scrollTop: ele.offset().top }, 1000);
    }
    var Sage = {
        // All pages
        'common': {
            init: function() {
                var search = $('.product-search');
                search.click(function(e) {
                    search.toggleClass('clicked');
                });
                $('#responsive_menu_pro .menu-item-type-post_type_archive.menu-item-object-botanic_el>a').click(function(e) {
                    e.preventDefault();
                    $(this).siblings('.responsive_menu_pro_append_link').click();
                    return false;
                })
            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        'single_botanic_el': {
            init: function() {

                setTimeout(function() { scrollToElement($('.botanic-el-content')) }, 0);
                $('.botanic-product-title').matchHeight();
            }
        },
        'page_template_chi_siamo': {
            init: function() {
                $('.open-popup-link').magnificPopup({
                    type: 'inline',
                    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
                });
            }
        },
        'post_type_archive_botanic_el': {
            init: function() {
                $('.botanic-product-title').matchHeight();
            }
        },
        'tax_product_cat': {
            init: function() {
                $('.woocommerce-LoopProduct-link>h3').matchHeight();
                $(window).resize(function() { $('.woocommerce-LoopProduct-link>h3').matchHeight(); });

            }
        },
        'post_type_archive_product': {
            init: function() {
                $('.woocommerce-LoopProduct-link>h3').matchHeight();
                $(window).resize(function() { $('.woocommerce-LoopProduct-link>h3').matchHeight(); });

            }
        },
        'single_product': {
            init: function() {
                $('.related.products .product h3').matchHeight();
                $(window).resize(function() { $('.related.products .product h3').matchHeight(); });
                $(document.body).on('added_to_cart', function(event, fragments, cart_hash) {
                    if (fragments['div.product-messages']) {
                        var messages = JSON.parse(fragments['div.product-messages']);
                        messages.forEach(function(message, i) {
                            $('main > #container').prepend(message);
                        });
                        $(window).scrollTo($('#container'), 600);
                    }
                })
            }
        },
        'woocommerce_cart': {
            init: function() {
                function showCAP() {
                    if ($('#calc_shipping_state').val() !== '') {
                        $('#calc_shipping_postcode').show();
                    } else {
                        $('#calc_shipping_postcode').hide();
                    }
                }
                $('#calc_shipping_state').on('change', showCAP);
                showCAP();
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
