<footer class="content-info">
  <nav class="nav-footer">
      <?php
      if (has_nav_menu('footer_navigation')) :
        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav o-list-bare']);
      endif;
    ?>
  </nav>
<div>  <div class="contacts-footer">
  <?php the_field('footer_contacts', 'option'); ?>
  <p> <?php echo wp_get_attachment_image( get_field('footer_logo', 'option'),'full'); ?></p>

  </div>
  <div class="contact-social">
     <?php the_field('footer_social', 'option'); ?>
  </div></div>
</footer>
<div class="footer-last-line"><div>&copy2017 Cosmeteria Verde - All rights reserved</div>
<div><a href="http://www.menthalia.com/" style="text-decoration: none; color:#000;">Web Agency Menthalia</a></div>  </div>
