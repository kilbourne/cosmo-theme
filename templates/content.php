<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
  <?php  //echo get_the_post_thumbnail() !== '' ?get_the_post_thumbnail():wc_placeholder_img(  );?>
    <?php the_excerpt(); ?>
  </div>
</article>
