<?php  use Roots\Sage\Extras; ?>
<header class="banner site-header">
  <div class="site-header-line">
     <div class="left"><?php echo do_shortcode('[responsive_menu_pro]' ).Extras\bios_wc_link(); ?></div>
     <div class="right"><?php echo
     //do_shortcode('[woocommerce_product_search]').
     Extras\sk_wcmenucart(); echo Extras\bios_lang_sel(); ?></div>
  </div>
  <div class="site-header-image"><?php   Extras\get_header_full_image(); Extras\complex_claim();?></div>
  <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav o-list-bare header-nav']);
      endif;
      ?>
  </nav>
  <div class="logo-mobile"><div class="logo"><a href="<?php echo home_url(); ?>">Logo</a></div></div>
</header>

