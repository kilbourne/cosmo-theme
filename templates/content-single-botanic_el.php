<?php
$args=[
'post_type'=>'botanic_el',
'posts_per_page'=>-1,
];
$botanic_query = new WP_Query( $args );
if($botanic_query->have_posts()){
echo '<ul class="botanic-el-list o-list-bare">';
while ( $botanic_query->have_posts() ) :
  $botanic_query->the_post();
$image =get_the_post_thumbnail();
      $image =  $image !== '' ? $image :  wc_placeholder_img( 'full' );
  echo '<li class="botanic-element"><a href="'.get_the_permalink().'" class="botanic-el-link">
    <div class="botanic-el-img">' . $image . '</div>
    <div class="botanic-el-title">' . get_the_title() . '</div>
  </a></li>';
endwhile;
echo '</ul>';
}
// Ripristina Query & Post Data originali
wp_reset_query();
wp_reset_postdata();
$nome_scientifico=get_field('nome_scienfitico');
$image =get_the_post_thumbnail();
      $image =  $image && $image !== '' ? $image :  wc_placeholder_img( 'full' );

?>
<div class="botanic-el-content">
  <div class="botanic-el-content-title"><h1><?php echo get_the_title().(( $nome_scientifico && $nome_scientifico !=='')?'<span>( '. $nome_scientifico .' )</span>':''); ?></h1></div>
   <div class="botanic-el-content-description"><?php the_content(); ?> </div>
   <div class="botanic-el-content-img"><?php echo $image; ?></div>
</div>
<?php
$product_query = get_field('relation_products_ingredients');

if($product_query){

echo '<div class="botanic-product-wrapper"><h3 class="botanic-element-section-title">'.__('Discover the products that contain','sage').' '.get_the_title().'</h3><ul class="botanic-product-list o-list-bare">';
foreach ( $product_query as $key => $product ) :
  $ID=$product->ID;
$image =get_the_post_thumbnail( $ID);
      $image =  $image !== '' ? $image :  wc_placeholder_img( 'full' );
  echo '<li class="botanic-product"><a href="'.get_the_permalink($ID).'" class="botanic-product-link">
    <div class="botanic-product-img">' . $image . '</div>
    <div class="botanic-product-title">' . get_the_title( $ID) . '</div>
  </a></li>';
endforeach;
echo '</ul></div> ';
}
