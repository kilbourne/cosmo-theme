<?php the_field('homepage_headline','option'); ?>

<?php
$p_cats = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
) );

if($p_cats){
  echo '<ul class="home-cats-list">';
  foreach ($p_cats as $key => $cat) {
    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
    $image = wp_get_attachment_image(  $thumbnail_id, 'full' );
ob_start();
    echo '<li class="home-cat"><a href="'.get_term_link($cat->term_id).'">'.$image.'<div class="home-cat-title">';
    echo get_field('home_title','product_cat_'.$cat->term_id);
    echo '</div></a></li>';
    //$output = ob_get_contents();
     ob_end_flush();
    //echo  $output ;
  }
  echo ' </ul>';
}
$ecl_text=get_field('homepage_eccelenze_testo','option');
$ecl_image=get_field('homepage_eccelenze_image','option');
?>
<div class="home-ecl-wrapper">
   <div class="ecl-image"><a href="<?php echo get_post_type_archive_link( 'botanic_el' ); ?> "><?php echo wp_get_attachment_image( $ecl_image,'full' ); ?></a> </div>
   <a href="<?php echo get_post_type_archive_link( 'botanic_el' ); ?> "><?php echo $ecl_text; ?></a>
 </div>
<?php
update_option( '_a_category_pricing_rules', array (
  'set_set_5901ddec5f363' =>
  array (
    'admin_title' => 'Corpo',
    'collector' =>
    array (
      'type' => 'cat',
      'args' =>
      array (
        'cats' =>
        array (
          0 => '15',
          1 => '7',
        ),
      ),
    ),
    'conditions_type' => 'all',
    'conditions' =>
    array (
      1 =>
      array (
        'type' => 'apply_to',
        'args' =>
        array (
          'applies_to' => 'everyone',
          'roles' =>
          array (
            0 => 'customer',
          ),
        ),
      ),
    ),
    'mode' => 'continuous',
    'targets' =>
    array (
      0 => '15',
      1 => '7',
    ),
    'date_from' => '',
    'date_to' => '',
    'blockrules' =>
    array (
      1 =>
      array (
        'from' => '',
        'adjust' => '',
        'type' => 'fixed_adjustment',
        'amount' => '',
        'repeating' => 'no',
      ),
    ),
    'rules' =>
    array (
      1 =>
      array (
        'from' => '2',
        'to' => '2',
        'type' => 'percentage_discount',
        'amount' => '10',
      ),
    ),
  ),
  'set_set_59098d97b1c96' =>
  array (
    'admin_title' => 'capelli',
    'collector' =>
    array (
      'type' => 'cat',
      'args' =>
      array (
        'cats' =>
        array (
          0 => '14',
          1 => '10',
        ),
      ),
    ),
    'conditions_type' => 'all',
    'conditions' =>
    array (
      1 =>
      array (
        'type' => 'apply_to',
        'args' =>
        array (
          'applies_to' => 'everyone',
          'roles' =>
          array (
            0 => 'customer',
          ),
        ),
      ),
    ),
    'mode' => 'continuous',
    'targets' =>
    array (
      0 => '14',
      1 => '10',
    ),
    'date_from' => '',
    'date_to' => '',
    'blockrules' =>
    array (
      1 =>
      array (
        'from' => '',
        'adjust' => '',
        'type' => 'fixed_adjustment',
        'amount' => '',
        'repeating' => 'no',
      ),
    ),
    'rules' =>
    array (
      1 =>
      array (
        'from' => '2',
        'to' => '2',
        'type' => 'percentage_discount',
        'amount' => '10%',
      ),
    ),
  ),
  'set_set_59098d9a98d93' =>
  array (
    'admin_title' => 'Viso',
    'collector' =>
    array (
      'type' => 'cat',
      'args' =>
      array (
        'cats' =>
        array (
          0 => '17',
          1 => '11',
        ),
      ),
    ),
    'conditions_type' => 'all',
    'conditions' =>
    array (
      1 =>
      array (
        'type' => 'apply_to',
        'args' =>
        array (
          'applies_to' => 'everyone',
          'roles' =>
          array (
            0 => 'customer',
          ),
        ),
      ),
    ),
    'mode' => 'continuous',
    'targets' =>
    array (
      0 => '17',
      1 => '11',
    ),
    'date_from' => '',
    'date_to' => '',
    'blockrules' =>
    array (
      1 =>
      array (
        'from' => '',
        'adjust' => '',
        'type' => 'fixed_adjustment',
        'amount' => '',
        'repeating' => 'no',
      ),
    ),
    'rules' =>
    array (
      1 =>
      array (
        'from' => '2',
        'to' => '2',
        'type' => 'percentage_discount',
        'amount' => '10',
      ),
    ),
  ),
  'set_set_59098d9ca09f0' =>
  array (
    'admin_title' => 'Mani',
    'collector' =>
    array (
      'type' => 'cat',
      'args' =>
      array (
        'cats' =>
        array (
          0 => '16',
          1 => '9',
        ),
      ),
    ),
    'conditions_type' => 'all',
    'conditions' =>
    array (
      1 =>
      array (
        'type' => 'apply_to',
        'args' =>
        array (
          'applies_to' => 'everyone',
          'roles' =>
          array (
            0 => 'customer',
          ),
        ),
      ),
    ),
    'mode' => 'continuous',
    'targets' =>
    array (
      0 => '16',

      1 => '9',
    ),
    'date_from' => '',
    'date_to' => '',
    'blockrules' =>
    array (
      1 =>
      array (
        'from' => '',
        'adjust' => '',
        'type' => 'fixed_adjustment',
        'amount' => '',
        'repeating' => 'no',
      ),
    ),
    'rules' =>
    array (
      1 =>
      array (
        'from' => '2',
        'to' => '2',
        'type' => 'percentage_discount',
        'amount' => '10',
      ),
    ),
  ),
)); ?>
