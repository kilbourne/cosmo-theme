<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}
?>
<li <?php wc_product_cat_class( '', $category ); ?>>
  <?php
  /**
   * woocommerce_before_subcategory hook.
   *
   * @hooked woocommerce_template_loop_category_link_open - 10
   */
  //do_action( 'woocommerce_before_subcategory', $category );

  /**
   * woocommerce_before_subcategory_title hook.
   *
   * @hooked woocommerce_subcategory_thumbnail - 10
   */
  //do_action( 'woocommerce_before_subcategory_title', $category );

  /**
   * woocommerce_shop_loop_subcategory_title hook.
   *
   * @hooked woocommerce_template_loop_category_title - 10
   */
  //do_action( 'woocommerce_shop_loop_subcategory_title', $category );

  /**
   * woocommerce_after_subcategory_title hook.
   */
  //do_action( 'woocommerce_after_subcategory_title', $category );

  /**
   * woocommerce_after_subcategory hook.
   *
   * @hooked woocommerce_template_loop_category_link_close - 10
   */
  //do_action( 'woocommerce_after_subcategory', $category );
  ?>
  <div class="row">
  <?php
  do_action( 'woocommerce_before_subcategory', $category );
  do_action( 'woocommerce_before_subcategory_title', $category );
  do_action( 'woocommerce_after_subcategory', $category );
  ?>
  <div class="category-products">
  <?php
//do_action( 'woocommerce_before_subcategory', $category );
//do_action( 'woocommerce_shop_loop_subcategory_title', $category );
//do_action( 'woocommerce_after_subcategory', $category );
  $args=[
  'post_type'=>'product',
  'product_cat'=>$category->slug,
  ];
  $cat_products=new WP_Query($args);
  if($cat_products->have_posts()){
    woocommerce_product_loop_start();


    while ($cat_products->have_posts() ) {
      $cat_products->the_post();
wc_get_template(
'content-product.php');
    }
    wp_reset_postdata();
  }
  woocommerce_product_loop_end();
  ?>
  </div>
  </div>
</li>
