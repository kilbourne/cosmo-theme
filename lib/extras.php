<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

add_action( 'init', function() {

  /**
   * Post Type: Elementi Botanici.
   */

  $labels = array(
    "name" => __( 'Elementi Botanici', 'sage' ),
    "singular_name" => __( 'Elemento Botanico', 'sage' ),
  );

  $args = array(
    "label" => __( 'Elementi Botanici', 'sage' ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => false,
    "rest_base" => "",
    "has_archive" => true,
    "show_in_menu" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => __("botanic_excellence",'sage'), "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor", "thumbnail" ),
  );

  register_post_type( "botanic_el", $args );
}
);

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title'  => __('Theme General Settings','sage'),
    'menu_title'  => __('Theme Settings','sage'),
    'parent_slug' => 'themes.php',
    'capability'  => 'edit_posts'
  ));
}

function get_header_full_image(){
  $image=false;
  if(is_product() || is_checkout() || is_cart()) return $image;
  if(is_shop()){
    $image = get_the_post_thumbnail(get_page_by_title('Prodotti'),'full');
  }elseif(is_page() ){
    $image= get_the_post_thumbnail(get_the_ID(),'full');
  }elseif(is_product()){

      $cats=get_the_terms( get_the_ID(), 'product_cat' );
      $thumb_id = get_woocommerce_term_meta( $cats[0]->term_id, 'thumbnail_id', true );

      $image = wp_get_attachment_image(  $thumb_id,'full' );
  }
  if(!$image || $image==='') $image=wp_get_attachment_image( get_field('header_image','option'), 'full' ).get_field('homepage_image_claim','option');
  if($image)echo $image;
}

function sk_wcmenucart($text=true) {

  // Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
  if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )  )
    return ;

  ob_start();
    global $woocommerce;
    $viewing_cart = __('Cart', 'sage');
    $cart_url = $woocommerce->cart->get_cart_url();
    $cart_contents_count = $woocommerce->cart->cart_contents_count;
    $cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'sage'), $cart_contents_count);
    // Uncomment the line below to hide nav menu cart item when there are no items in the cart


        $menu_item = '<a '.($text?'class="wcmenucart-contents"':'').' href="'. $cart_url .'" title="'. ($text?$viewing_cart:'') .'" '.(!( $cart_contents_count > 0 )?' hidden':'').'>';


      $menu_item .= '<i class="fa fa-shopping-cart"></i> ';

      $menu_item .= '<span class="wcmenucart-text">(<span class="cart-length">' . $cart_contents_count . '</span>) '.($text?$viewing_cart:'');
      $menu_item .= '</span></a>';
    // Uncomment the line below to hide nav menu cart item when there are no items in the cart
      echo $menu_item;


  $social = ob_get_clean();
  return $social;

}

function bios_wc_link(){
    if (is_user_logged_in()) {
      return '<div class="account-link"><div><a href="'.get_permalink(woocommerce_get_page_id('myaccount')). '">'.__('Profile', 'sage' ).' </a> | <a href="'. esc_url( wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ) ) .'">'.__('Logout', 'sage' ).'</a></div></div>';
    }elseif (!is_user_logged_in() ) {
      return '<div class="account-link"><div><a href="'.get_permalink(woocommerce_get_page_id('myaccount')). '">'.__('Login', 'sage' ).' </a> | <a href="'.get_permalink(woocommerce_get_page_id('myaccount')). '?action=register">'.__('Register', 'sage' ).' </a></div></div>';
    }
}

function cosmo_get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
  if ( ! empty( $args ) && is_array( $args ) ) {
    extract( $args );
  }
  $located = wc_locate_template( $template_name, $template_path, $default_path );
  if ( ! file_exists( $located ) ) {

    return;
  }
  // Allow 3rd party plugin filter template file from their plugin.
  $located = apply_filters( 'wc_get_template', $located, $template_name, $args, $template_path, $default_path );
  do_action( 'woocommerce_before_template_part', $template_name, $template_path, $located, $args );
  include( $located );
  do_action( 'woocommerce_after_template_part', $template_name, $template_path, $located, $args );
}

add_filter( 'posts_clauses', function ( $clauses, $wp_query ) {
  global $wpdb;

  if ( isset( $wp_query->query['orderby'] ) && 'product_cat' == $wp_query->query['orderby'] ) {

    $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;

    $clauses['where'] .= " AND (taxonomy = 'product_cat' OR taxonomy IS NULL)";
    $clauses['groupby'] = "object_id";
    $clauses['orderby']  = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
    $clauses['orderby'] .= ( 'ASC' == strtoupper( $wp_query->get('order') ) ) ? 'ASC' : 'DESC';
  }

  return $clauses;
}, 10, 2 );

add_filter( 'wp_nav_menu_items', function($items,$args){
if($args->menu_id !== 'responsive_menu_pro_menu') return $items;
  ob_start();
  echo bios_wc_link().sk_wcmenucart().bios_lang_sel();
  $rest=ob_get_clean();
  return $rest.$items;
},10,2 );
if (!in_array('administrator', wp_get_current_user()->roles)) {

    add_filter('screen_options_show_screen', '__return_false');
}
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
if (!empty($GLOBALS['sitepress'])) {
    add_action('wp_head', function () {
        remove_action(
            current_filter(),
            array($GLOBALS['sitepress'], 'meta_generator_tag')
        );
    },
        0
    );
}
add_action('init', function () {
    remove_action('wp_head', 'Roots\\Soil\\CleanUp\\rel_canonical');
}, 15);

function bios_lang_sel($div='',$echo=false){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    foreach($languages as $l){

      if(!$l['active']) {$langs[] = '<a href="'.$l['url'].'" data-lang="' . $l['language_code'] . '">'.strtoupper($l['code']).'</a>';}
      else{ $langs[]='<span class="active" >'.strtoupper($l['code']).'</span>';}
    }
    $result=join($div, $langs);
    if($echo) {echo '<div  id="lang_sel" class="lansel">
          '.$result.'
        </div>    ';}
    else{return '<div  id="lang_sel" class="lansel">
          '.$result.'
        </div>    ';}
  }
}






function cv_action($function_name,$arr){
  array_walk($arr, function(&$el, $action)use($function_name) {
      if(is_string($el) && $el==='all' ){
        remove_all_actions($action);
      }else{
            array_walk($el, function( $priority,$func_name) use($function_name,$action){
              if($priority === false ){ call_user_func($function_name, $action,$func_name); }
              else{call_user_func($function_name, $action,$func_name,$priority);}
            });
      }
   });
}
function cv_remove_action($arr){

    cv_action('remove_action',$arr);

}
function cv_add_action($arr){
  cv_action('add_action',$arr);
}

function cosmo_product_info(){
  $field_keys=['modo_duso','utilizzo'];
$ingredients=get_field('relation_products_ingredients');

  ?>
  <div class="product-info">
     <div class="ingredients"><h5><?php  _e('Ingredients','sage') ?></h5>
     <p>  <?php  $ingredients=array_map( function($el){
      return '<a href="'.get_permalink($el->ID).'">'.$el->post_title.'</a>';
      }, $ingredients);
     echo implode(',',$ingredients);
      ?></p> </div>
      <?php  array_map( function($el){
        $field=get_field_object($el);

        $value=$field['value'];
        if($value && $value !=='')
      echo '<div class="'.$el.'"><h5>'.__($field['label'],'sage').'</h5>
      <div> '.$value.'</div></div>';
      }, $field_keys)
      ?>
   </div>
  <?php
}
function cosmo_product_action(){
  ?>
  <div class="product-payment">
  <h5 class="product-info-wrapper"><?php _e('Buy Information','sage'); ?></h5>
  <?php   do_action('inside_cosmo_product_action' ); ?>
  </div>
  <?php

}

function cosmo_buy_note(){
  echo '<div class="product-disponibility">  <p>'.__('Immediate availability: delivery in 3-5 days','sage').'</p> <p>'.__('10% discount on the total if you buy 2 products of the same category','sage').'</p>';
  // <p>'.__('30% discount on the total if you buy 3 products','sage').'</p>
echo '</div>';
}
function cosmo_shipping_note(){
  echo '<div class="product-shipping">
   <p>'.__('FREE SHIPPING FOR ORDERS OVER € 19.99','sage').'</p>
  </div>';
}

function cosmo_product_ingredients(){
  $ingredients=get_field('relation_products_ingredients');

  if($ingredients){
    echo '<ul  class="product-ingredients-list">';
    foreach ($ingredients as $key => $ingredient) {
      $image =get_the_post_thumbnail($ingredient->ID,'full');
      $image =  $image !== '' ? $image :  wc_placeholder_img( 'full' );
      echo '<li class="product-ingredient"><a href="'.get_the_permalink($ingredient->ID).'">'.$image.'<h5 class="ingredient-title">'.get_the_title($ingredient->ID).'</h5></a></li>';
    }
    echo '</ul>';
  }
}
function cosmo_formato_attribute(){
  if( get_field('formato') ) echo '<span class="formato">'.__('Size','sage').': '.get_field('formato').'</span>';
}
function cosmo_facebook(){
  echo '<div id="fb-root"></div>
<script  >(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/'.get_locale().'/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));</script><div class="social-container"><div  >
  <div class="fb-share-button" data-href="' . get_permalink() . '" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_permalink() . '&src=sdkpreparse') . '" >' . __('Share', 'sage') . '</a></div></div></div>';
}
function cosmo_add_to_cart(){
  echo '<div class="payment-form-wrapper">'; echo woocommerce_template_single_add_to_cart(); echo'</div>';
}
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );
add_filter('woocommerce_subcategory_count_html','__return_empty_string');
$actions_to_remove = [
  'woocommerce_single_product_summary' => [
    'woocommerce_template_single_rating'=>10,
    'woocommerce_template_single_price'=>10,
    'woocommerce_template_single_add_to_cart'=>30,
  ],
  'woocommerce_after_single_product_summary'=>[
    "woocommerce_output_product_data_tabs" => 10,
    "woocommerce_upsell_display" => 15,
    //"woocommerce_output_related_products" => 20
  ],
  'woocommerce_after_shop_loop_item'=>[
  "woocommerce_template_loop_add_to_cart" => 10
  ],
  'woocommerce_before_shop_loop'=>'all',

'woocommerce_cart_collaterals' =>[ 'woocommerce_cross_sell_display' => false],
'woocommerce_after_shop_loop_item_title'=>[ 'woocommerce_template_loop_price' => 10]
];

$actions_to_add = [
    'woocommerce_after_single_product_summary'=>[
        __NAMESPACE__ . "\\cosmo_product_info" => 10,
        __NAMESPACE__ . "\\cosmo_product_action" => 10,
        __NAMESPACE__ . "\\cosmo_product_ingredients" => 10,
    ],
    'woocommerce_single_product_summary'=>[
    __NAMESPACE__ . '\\cosmo_add_to_cart'=>21,
    ],
    'inside_cosmo_product_action'=>[
__NAMESPACE__ . '\\cosmo_buy_note'=>10,
     __NAMESPACE__ . '\\cosmo_shipping_note'=>31,
    ],
    'woocommerce_after_add_to_cart_button'=>[

    ],
    'woocommerce_product_meta_end'=>[
      __NAMESPACE__ . '\\cosmo_formato_attribute'=>10,
    ],
    'woocommerce_share'=>[
      __NAMESPACE__ . '\\cosmo_facebook'=>10,
    ],
    'woocommerce_before_single_product_summary'=>[
      __NAMESPACE__ . '\\cosmo_mobile_title'=>5
    ],
   'woocommerce_after_cart' =>[ 'woocommerce_cross_sell_display' => 100],
];
cv_remove_action($actions_to_remove);
cv_add_action($actions_to_add);


function cosmo_mobile_title(){
  return the_title( '<h1 itemprop="name" class="product_title entry-title mobile-title" >', '</h1>' );
}


add_filter('woocommerce_add_to_cart_fragments', __NAMESPACE__ . '\\woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    ?>
  <a class="wcmenucart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('Cart', 'sage');?>" <?php echo !( WC()->cart->get_cart_contents_count() > 0 )?' hidden':''; ?>><i class="fa fa-shopping-cart"></i><span class="wcmenucart-text">(<span class="cart-length">
  <?php echo WC()->cart->get_cart_contents_count(); ?> </span>) <?php _e('Cart', 'sage')?></span></a>


  <?php

    $fragments['a.wcmenucart-contents'] = ob_get_clean();

    return $fragments;
}

//add_filter( 'the_seo_framework_ogtitle_output', function($a,$b){ return $b;},10,2 );

function complex_claim(){
if(is_page_template('contact-form.php')) echo '<div class="contacts-complex-claim">
'.get_field('contatti_claim','option').'
    <img src="'.get_stylesheet_directory_uri().'/dist/images/logopisa254x166.png" alt="">
  </div>';
}

add_filter( 'the_seo_framework_pro_add_title',  __NAMESPACE__ . '\\my_special_page_title', 10, 3 );
/**
 * Alters the title on special conditions.
 *
 * @param string $title The current title.
 * @param array $args The title generation arguments.
 * @param bool $escape Whether the title is being escaped.
 * @return string Title. Does not need to be escaped.
 */
function my_special_page_title( $title= '', $args = array(), $escape = true ) {

    /**
     * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
     */
    if ( is_post_type_archive( 'botanic_el' ) ) {
        $title = __('Italian botanic excellences','sage').' | Cosmeteria Verde';

    }

    return $title;
}
add_filter( 'the_seo_framework_description_output',  __NAMESPACE__ . '\\my_special_description', 10, 2 );
add_filter( 'the_seo_framework_ogdescription_output',  __NAMESPACE__ . '\\my_special_description', 10, 2 );
add_filter( 'the_seo_framework_twitterdescription_output',  __NAMESPACE__ . '\\my_special_description', 10, 2 );
/**
 * Alters the description on special conditions.
 *
 * @param string $description The current description.
 * @param int $id The Post, Page or Term ID.
 * @return string Description. Does not need to be escaped.
 */
function my_special_description( $description = '', $id = 0 ) {

    /**
     * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
     */
    if ( is_post_type_archive( 'botanic_el' ) ) {
        $description = __('For Cosmeteria Verde products we use Italian botanical excellence. Rich in active ingredients, they’re precious for skin care and health.','sage');
    }

    return $description;
}

add_filter('woocommerce_add_to_cart_fragments', function ($fragments)
{
    $messages=array();

    $cart = WC()->cart;

    if($cart->cart_contents_total < 19.99) $messages[] = '<div class="woocommerce-info">'. wp_kses_post( __("Continue to shop. Free shipping for orders over € 19.99 ","sage") ).'</div>';

    if($cart->cart_contents_count<2){
$messages[] = '<div class="woocommerce-info">'. wp_kses_post( __("Add an other product of the same category. For you 10% discount on total","sage") ).'</div>';
    }
    //elseif($cart->cart_contents_count<3) {$messages[] = '<div class="woocommerce-info">'. wp_kses_post( __("Add an other product. For you 30% discount on total","sage") ).'</div>';}
$messages[]= '<div class="woocommerce-message">'. wp_kses_post( __("The product has been added to the cart. ","sage") ).'<a href="'.esc_url( wc_get_page_permalink( 'cart' ) ).'">'.__('See the cart','sage').'</a>'.__(' or ','sage').'<a href="'.esc_url( wc_get_page_permalink( 'shop' ) ).'">'.__('continue to shop.','sage').'</a>'.'</div>';
    $fragments['div.product-messages'] = json_encode($messages);


    return $fragments;
});

function my_hide_shipping_when_free_is_available( $rates ) {
  $free = array();
  foreach ( $rates as $rate_id => $rate ) {
    if ( 'free_shipping' === $rate->method_id ) {
      $free[ $rate_id ] = $rate;
      break;
    }
  }
  return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', __NAMESPACE__ . '\\my_hide_shipping_when_free_is_available', 100 );

add_action('woocommerce_before_customer_login_form',function (){
if(isset($_GET['action'])=='register'){
woocommerce_get_template( 'myaccount/form-register.php' );
}else{
  woocommerce_get_template( 'myaccount/form-login1.php' );
}
}, 2);
