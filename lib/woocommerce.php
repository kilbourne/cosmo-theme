<?php
/*
remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);
add_filter( 'woocommerce_show_page_title', '__return_false' ) ;
remove_all_actions( 'woocommerce_before_shop_loop');
remove_all_actions( 'woocommerce_after_shop_loop_item_title' );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' , 10 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 7 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_all_filters('woocommerce_product_tabs');
add_filter( 'woocommerce_product_tabs', function($tabs){
    $default=['eccellenze','azione','risultati'];
    foreach ($default as $key => $value) {
        $key=$key*10;
        $tabs[]=array(
                'title'    => __( ucfirst ($value ) , 'sage' ),
                'priority' => $key,
                'callback' => 'cosmo_tabs_default_callback',
            );
    }
    return $tabs;
});

function cosmo_tabs_default_callback($key,$tab){

        echo get_field(lcfirst($tab['title']));
}
*/

function content_product_by_cat(){


$args=[
"post_type"=>"product",
"posts_per_page"=>-1,
"orderby"=>'product_cat'
];
if(!is_shop()) {
    $term = get_queried_object();
    $args['product_cat']=$term->slug;
    unset($args['orderby']);
}
$my_q =new WP_Query($args);
$init_cat=null;
 while ($my_q ->have_posts() ) {
    $my_q->the_post();

    $category = wp_get_post_terms( get_the_ID(), 'product_cat' )[0];

    $cat_id = $category->term_id;
    if($cat_id !== $init_cat && $init_cat!==null ){
        woocommerce_product_loop_end();
        echo '</div></div>';
    }
    if($cat_id !== $init_cat){
        echo '<div class="row">';

  do_action( 'woocommerce_before_subcategory', $category );
do_action( 'woocommerce_shop_loop_subcategory_title', $category );
  do_action( 'woocommerce_after_subcategory', $category );

  echo '<div class="category-products">';

//do_action( 'woocommerce_before_subcategory', $category );
//do_action( 'woocommerce_before_subcategory_title', $category );
//
//do_action( 'woocommerce_after_subcategory', $category );
 woocommerce_product_loop_start();
    }


    wc_get_template('content-product.php');
 wp_reset_postdata();

    $init_cat = $cat_id;
}

};

