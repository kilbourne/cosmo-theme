
<?php
/**
 * Template Name: Chi siamo
 */
?><?php while (have_posts()) : the_post(); ?>
  <?php //get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile;
$args=[
'post_type'=>'botanic_el',
'posts_per_page'=>-1,
];
$botanic_query = new WP_Query( $args );
if($botanic_query->have_posts()){
echo '<ul class="botanic-el-list o-list-bare">';
while ( $botanic_query->have_posts() ) :
  $botanic_query->the_post();
$image =get_the_post_thumbnail();
      $image =  $image !== '' ? $image :  wc_placeholder_img( 'full' );
  echo '<li class="botanic-element"><a href="'.get_the_permalink().'" class="botanic-el-link">
    <div class="botanic-el-img">' . $image . '</div>
    <div class="botanic-el-title">' . get_the_title() . '</div>
  </a></li>';
endwhile;
echo '</ul>';
}
// Ripristina Query & Post Data originali
wp_reset_query();
wp_reset_postdata();
?>
<?php  $field = get_field_object('descrizione_iso_9001','option'); ?>
<div id="<?php echo $field['name']   ?>" class="white-popup mfp-hide">
  <?php echo $field['value']   ?>
</div>
<?php  $field = get_field_object('descrizione_iso_22716','option');  ?>
<div id="<?php echo $field['name']   ?>" class="white-popup mfp-hide">
  <?php echo $field['value']   ?>
</div>
<?php  $field = get_field_object('descrizione_iso_14001','option');  ?>
<div id="<?php echo $field['name']   ?>" class="white-popup mfp-hide">
  <?php echo $field['value']   ?>
</div>
