<?php
/**
 * Template Name: Contacts
 */
?>
<div class="contact">
    <h3> Invia la tua richiesta</h3>
    <div class="container">
        <div class="servizio-clienti-wrapper"><div>
            <p>Numero di telefono</p>
            <p class="italics">servizio clienti</p>
            <p>035 461634</p>
        </div></div>
        <div class="contacts">
            <p><strong>VALETUDO SRL</strong>
<span>Divisione Cosmeteria Verde</span>
<span>Via Ghiaie, 6 - 24030</span>
<span>Presezzo BG</span></p>
        </div>
        <div class="contact-form"><?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]') ?></div>
     </div>
</div>
<div class="map">
 <h3>La nostra sede </h3>
 <div class="map-container">
   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2787.0234212701776!2d9.576863315998034!3d45.6905019791042!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478152fdb0fd0475%3A0xcd9db6e230a093bf!2sVia+Ghiaie%2C+6%2C+24030+Presezzo+BG!5e0!3m2!1sit!2sit!4v1492076416018" width="1280" height="353" frameborder="0" style="border:0" allowfullscreen></iframe>
 </div>
</div>
